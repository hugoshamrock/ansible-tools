```bash

# build docker image
docker build -t ansible-tools .

# run docker in docker (dind)
docker run --rm --pull=always --privileged --name=molecule --detach -v $(pwd):/app hugoshamrock/ansible-tools:latest
docker exec -it molecule sh

# run docker outside of docker (dood)
docker run --rm --pull=always -v /var/run/docker.sock:/var/run/docker.sock -it hugoshamrock/ansible-tools:latest sh

# prepare molecule role
molecule init role acme.myrole -d docker

# prepare molecule linters
echo -e "lint: |\n  set -e\n  yamllint .\n  ansible-lint ." >> /app/myrole/molecule/default/molecule.yml

# testing molecule testing
cd /app/myrole
molecule create
molecule test
molecule converge
molecule lint
molecule login
molecule destroy

# remove docker container
docker rm molecule -f

```
