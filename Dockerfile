FROM docker:20.10.22-dind-alpine3.17
WORKDIR /app

COPY requirements.txt .

RUN apk add --no-cache py3-pip \
 && apk add --no-cache py3-ruamel.yaml.clib \
 && apk add --no-cache git \
 && pip3 install --no-cache-dir --requirement requirements.txt \
 && rm -rf /var/cache/apk/*
